#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=a
DEBUGGABLE_SUFFIX=a
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Delay_ms.c LCDbusy.c LCDcommand.c LCDinit.c LCDletter.c LCDpulseEnableBit.c LCDputs.c LCDsend.c Delay_short.c delay32.s OLEDinit.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Delay_ms.o ${OBJECTDIR}/LCDbusy.o ${OBJECTDIR}/LCDcommand.o ${OBJECTDIR}/LCDinit.o ${OBJECTDIR}/LCDletter.o ${OBJECTDIR}/LCDpulseEnableBit.o ${OBJECTDIR}/LCDputs.o ${OBJECTDIR}/LCDsend.o ${OBJECTDIR}/Delay_short.o ${OBJECTDIR}/delay32.o ${OBJECTDIR}/OLEDinit.o
POSSIBLE_DEPFILES=${OBJECTDIR}/Delay_ms.o.d ${OBJECTDIR}/LCDbusy.o.d ${OBJECTDIR}/LCDcommand.o.d ${OBJECTDIR}/LCDinit.o.d ${OBJECTDIR}/LCDletter.o.d ${OBJECTDIR}/LCDpulseEnableBit.o.d ${OBJECTDIR}/LCDputs.o.d ${OBJECTDIR}/LCDsend.o.d ${OBJECTDIR}/Delay_short.o.d ${OBJECTDIR}/delay32.o.d ${OBJECTDIR}/OLEDinit.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Delay_ms.o ${OBJECTDIR}/LCDbusy.o ${OBJECTDIR}/LCDcommand.o ${OBJECTDIR}/LCDinit.o ${OBJECTDIR}/LCDletter.o ${OBJECTDIR}/LCDpulseEnableBit.o ${OBJECTDIR}/LCDputs.o ${OBJECTDIR}/LCDsend.o ${OBJECTDIR}/Delay_short.o ${OBJECTDIR}/delay32.o ${OBJECTDIR}/OLEDinit.o

# Source Files
SOURCEFILES=Delay_ms.c LCDbusy.c LCDcommand.c LCDinit.c LCDletter.c LCDpulseEnableBit.c LCDputs.c LCDsend.c Delay_short.c delay32.s OLEDinit.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33EV256GM102
MP_LINKER_FILE_OPTION=,--script=p33EV256GM102.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Delay_ms.o: Delay_ms.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Delay_ms.o.d 
	@${RM} ${OBJECTDIR}/Delay_ms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Delay_ms.c  -o ${OBJECTDIR}/Delay_ms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Delay_ms.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/Delay_ms.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDbusy.o: LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/LCDbusy.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDbusy.c  -o ${OBJECTDIR}/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDbusy.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDbusy.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDcommand.o: LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/LCDcommand.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDcommand.c  -o ${OBJECTDIR}/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDcommand.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDcommand.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDinit.o: LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/LCDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDinit.c  -o ${OBJECTDIR}/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDinit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDletter.o: LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/LCDletter.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDletter.c  -o ${OBJECTDIR}/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDletter.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDletter.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDpulseEnableBit.o: LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/LCDpulseEnableBit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDpulseEnableBit.c  -o ${OBJECTDIR}/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDpulseEnableBit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDpulseEnableBit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDputs.o: LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/LCDputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDputs.c  -o ${OBJECTDIR}/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDputs.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDsend.o: LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/LCDsend.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDsend.c  -o ${OBJECTDIR}/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDsend.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDsend.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Delay_short.o: Delay_short.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Delay_short.o.d 
	@${RM} ${OBJECTDIR}/Delay_short.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Delay_short.c  -o ${OBJECTDIR}/Delay_short.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Delay_short.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/Delay_short.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/OLEDinit.o: OLEDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/OLEDinit.o.d 
	@${RM} ${OBJECTDIR}/OLEDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  OLEDinit.c  -o ${OBJECTDIR}/OLEDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/OLEDinit.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/OLEDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/Delay_ms.o: Delay_ms.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Delay_ms.o.d 
	@${RM} ${OBJECTDIR}/Delay_ms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Delay_ms.c  -o ${OBJECTDIR}/Delay_ms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Delay_ms.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/Delay_ms.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDbusy.o: LCDbusy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDbusy.o.d 
	@${RM} ${OBJECTDIR}/LCDbusy.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDbusy.c  -o ${OBJECTDIR}/LCDbusy.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDbusy.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDbusy.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDcommand.o: LCDcommand.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDcommand.o.d 
	@${RM} ${OBJECTDIR}/LCDcommand.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDcommand.c  -o ${OBJECTDIR}/LCDcommand.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDcommand.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDcommand.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDinit.o: LCDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDinit.o.d 
	@${RM} ${OBJECTDIR}/LCDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDinit.c  -o ${OBJECTDIR}/LCDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDinit.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDletter.o: LCDletter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDletter.o.d 
	@${RM} ${OBJECTDIR}/LCDletter.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDletter.c  -o ${OBJECTDIR}/LCDletter.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDletter.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDletter.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDpulseEnableBit.o: LCDpulseEnableBit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDpulseEnableBit.o.d 
	@${RM} ${OBJECTDIR}/LCDpulseEnableBit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDpulseEnableBit.c  -o ${OBJECTDIR}/LCDpulseEnableBit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDpulseEnableBit.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDpulseEnableBit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDputs.o: LCDputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDputs.o.d 
	@${RM} ${OBJECTDIR}/LCDputs.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDputs.c  -o ${OBJECTDIR}/LCDputs.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDputs.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDputs.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/LCDsend.o: LCDsend.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LCDsend.o.d 
	@${RM} ${OBJECTDIR}/LCDsend.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  LCDsend.c  -o ${OBJECTDIR}/LCDsend.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/LCDsend.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/LCDsend.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/Delay_short.o: Delay_short.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Delay_short.o.d 
	@${RM} ${OBJECTDIR}/Delay_short.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  Delay_short.c  -o ${OBJECTDIR}/Delay_short.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/Delay_short.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/Delay_short.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/OLEDinit.o: OLEDinit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/OLEDinit.o.d 
	@${RM} ${OBJECTDIR}/OLEDinit.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  OLEDinit.c  -o ${OBJECTDIR}/OLEDinit.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/OLEDinit.o.d"      -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   
	@${FIXDEPS} "${OBJECTDIR}/OLEDinit.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/delay32.o: delay32.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay32.o.d 
	@${RM} ${OBJECTDIR}/delay32.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  delay32.s  -o ${OBJECTDIR}/delay32.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/delay32.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD3=1,-g,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/delay32.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/delay32.o: delay32.s  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/delay32.o.d 
	@${RM} ${OBJECTDIR}/delay32.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  delay32.s  -o ${OBJECTDIR}/delay32.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,-MD,"${OBJECTDIR}/delay32.o.d",--defsym=__MPLAB_BUILD=1,--no-relax$(MP_EXTRA_AS_POST)
	@${FIXDEPS} "${OBJECTDIR}/delay32.o.d"  $(SILENT)  -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: archive
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX} 
	${MP_AR} $(MP_EXTRA_AR_PRE)  -omf=elf -r dist/${CND_CONF}/${IMAGE_TYPE}/LCD.X.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
